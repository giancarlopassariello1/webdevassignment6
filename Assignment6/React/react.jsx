"use strict";

const {useState} = React;

function TextField( {myValue, onInput} ) {
    const [value, setValue] = useState(myValue);

    const handleInput = (e) => {
        setValue(e.target.value);
        onInput(e.target.value); 
    };

    return (<input type="text" value={value} onInput={ handleInput }/>);
}

function SearchButton( { myValue, setImageList } ) {
    
    function fetchImageList() {
            let url = `https://api.artic.edu/api/v1/artworks/search?q=${myValue}`;
            fetch(url)
                .then(response => {
                    if (!response.ok) {
                        throw new Error('Something went wrong');
                    }
                    return response.json();
                })
                .then((obj) => {
                    console.log(obj);
                    setImageList(obj.data);
                })
                .catch(err => {
                    console.error("Error: ", err);
                });
    }

    return (<button onClick={ (e) => {
                e.preventDefault();
                fetchImageList();}
            }>Search</button>);
}

function ArtworkDetails({ artwork }) {
    if (!artwork) {
      return <p>Search for an artwork to view details.</p>;
    }
  
    const [details, setDetails] = useState(null);
  
    function fetchArtworkDetails(id) {
      let url = `https://api.artic.edu/api/v1/artworks/${id}`;
      fetch(url)
        .then((response) => {
          if (!response.ok) {
            throw new Error('Something went wrong');
          }
          return response.json();
        })
        .then((data) => {
          setDetails(data.data);
        })
        .catch((error) => {
          console.error("Error fetching artwork details: ", error);
        });
    }
  
    fetchArtworkDetails(artwork.id);
  
    if (!details) {
      return null;
    }
  
    const imageUrl = `https://www.artic.edu/iiif/2/${details.image_id}/full/843,/0/default.jpg`;
  
    return (
      <div>
        <h2>{details.title}</h2>
        <p className="artist-info">{details.artist_display}</p>
        <img src={imageUrl} alt={details.title} />
        <p dangerouslySetInnerHTML={{ __html: details.description }} />
        <p>Medium: </p>
        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{details.medium_display}</p>
        <p>Dimension: </p>
        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{details.dimensions}</p>
      </div>
    );
}
   
function App() {
    const [ myValue, setMyValue ] = useState("");
    const [ imageList, setImageList ] = useState([]);
    const [ selectedArtwork, setSelectedArtwork ] = useState(null);

    function onInput(value) {
        setMyValue(value);
    }
    
    return (
        <div className="wrapper">
            <header>
                <h1>Art Institute of Chicago Search</h1>
                <form>
                    <TextField myValue="*Insert the author name*" onInput={onInput} />
                    <SearchButton myValue={ myValue } setImageList={ setImageList } />
                </form>
                <ul>
                  {imageList.map((image) => {
                        let className = "";
                        if (selectedArtwork && selectedArtwork.id === image.id) {
                            className = "selected";
                        }

                        return (<li key={image.id} className={className} onClick={() => setSelectedArtwork(image)}>{image.title}</li>);
                  })}
                </ul>
            </header>
            <main>
                <ArtworkDetails artwork={selectedArtwork} />
            </main>
        </div>
    )
}

ReactDOM.render(
    <App />,
    document.querySelector("#react-root")
);